# Krita Mockups Vector Library


The purpose of this repository is to create a set of tools to make creating mockups for Krita easier.

## Color palette

File: `KritaMockupsPalette.kpl`

To keep our widgets in the same color scheme, I added a palette that needs to be used when creating widgets (which is 'Krita Dark', but I must admit I have some issues with color management and colors might be slightly different).

## Widgets vector library

File: `mockup_library.svg`

Widgets included:

- push button (standard size + bigger)
- drop down button
- checkbox checked
- checkbox unchecked
- a few rectangles for background
- foreground text (for now I cannot edit it, but if I ungroup the push buttons, I can grab the text from there).

Additional:
- brush icon
- brush preset resource icon (with the grey background).


Issues:
- one cannot change outline etc. of rectangles
- scaling of buttons look weird (outlines)
- the foreground text cannot be edited (workaround: grab a button, ungroup, copy text and use/edit that one).
- the brush on the brush preset resource icon is a bit too dark

## Icons vector library

It doesn't exist yet, since it wasn't needed for me. There are a few icons in the widgets library that I found useful.
